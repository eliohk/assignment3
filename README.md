# Assignment 3 - Web Api & Database with Spring

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

In the third and final assignment of this course, we are tasked with creating a PostgreSQL database using Hibernate and exposing it through a deployed Web API.

The PostgreSQL database primarily consists of three tables: Movie, Character & Franchise. 
In addition, there are also relationships which reflect the described business rules in the assignment.

## Table of Contents

- [Install](#install)
- [Usage](#usage)
- [API](#api)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Install

```
• IntelliJ with Java 17
• Spring Web, Spring Data JPA, PostgreSQL, Lombok, Swagger
• PostgreSQL with PgAdmin
• Docker – replication of environment
```

## Usage

```
• Clone GitLab repository
• Open repository folder in IntelliJ
• Create a 'moviesdb' database in PgAdmin
• Make sure application.properties variables are correct according to your settings in PgAdmin
```

## API
The Web API uses RESTful conventions (it does not satisfy all the 6 constraints in order to be called a REST-API). The 
media type is JSON.

The Web API can be found in localhost, port 8080, starting from the path "api/v1/{dbTable}"

#### where ${dbTable} can be one of the following:
- [movies](http://localhost:8080/api/v1/movies)
- [characters](http://localhost:8080/api/v1/characters)
- [franchises](http://localhost:8080/api/v1/franchises)

More information and documentation can be found in Swagger/openAPI:
- [Swagger](http://localhost:8080/swagger-ui/index.html)

## Maintainers
- [@AdamKrason](https://github.com/AdamKrason)
- [@eliohk](https://github.com/eliohk)

## Contributing
Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

## License

MIT © 2023 Adam & Khoi
