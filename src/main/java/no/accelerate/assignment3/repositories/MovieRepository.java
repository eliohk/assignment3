package no.accelerate.assignment3.repositories;

import no.accelerate.assignment3.models.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * <h1>MovieRepository</h1>
 * A repository containing generic operations on a Movie-entity.
 *
 * @author  Adam Krason
 * @author  Khoi Le
 * @version 1.0
 * @since   2023-02-01
 */
@Repository
public interface MovieRepository extends JpaRepository<Movie, Integer> { }
