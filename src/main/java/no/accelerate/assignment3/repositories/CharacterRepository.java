package no.accelerate.assignment3.repositories;

import no.accelerate.assignment3.models.Character;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * <h1>CharacterRepository</h1>
 * A repository containing generic operations on a Character-entity.
 *
 * @author  Adam Krason
 * @author  Khoi Le
 * @version 1.0
 * @since   2023-02-01
 */
@Repository
public interface CharacterRepository extends JpaRepository<Character, Integer> { }