package no.accelerate.assignment3.services.character;

import no.accelerate.assignment3.models.Character;
import no.accelerate.assignment3.models.Movie;
import no.accelerate.assignment3.models.exceptions.CharacterNotFoundException;
import no.accelerate.assignment3.repositories.CharacterRepository;
import no.accelerate.assignment3.repositories.MovieRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * <h1>CharacterServiceImpl</h1>
 * A class which contain the implementation of operations on the 'character'-table and its business logic
 *
 * @author  Adam Krason
 * @author  Khoi Le
 * @version 1.0
 * @since   2023-02-01
 */
@Service
public class CharacterServiceImpl implements CharacterService {
    private final CharacterRepository characterRepository;
    private final MovieRepository movieRepository;

    /**
     * An implementation of the CharacterService needs to have two repositories injected
     * in order to provide the desired functionality and business logic
     *
     * @param characterRepository The character-repository containing generic operations on a character-table
     * @param movieRepository The movie-repository containing generic operations on a movie-table
     */
    public CharacterServiceImpl(CharacterRepository characterRepository, MovieRepository movieRepository) {
        this.characterRepository = characterRepository;
        this.movieRepository = movieRepository;
    }

    @Override
    public Character findById(Integer id) {
        return characterRepository.findById(id).orElseThrow(()
                -> new CharacterNotFoundException(id));
    }

    @Override
    public Collection<Character> findAll() {
        return characterRepository.findAll();
    }

    @Override
    public Character add(Character entity) {
        return characterRepository.save(entity);
    }

    @Override
    public Character update(Character entity) {
        return characterRepository.save(entity);
    }

    @Override
    public void deleteById(Integer id) {
        characterRepository.deleteById(id);
    }

    @Override
    public boolean exists(Integer id) {
        return characterRepository.existsById(id);
    }

    @Override
    public Collection<Movie> getMovies(Integer characterId) {
        return characterRepository.findById(characterId).get().getMovies();
    }

}
