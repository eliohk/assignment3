package no.accelerate.assignment3.services.character;

import no.accelerate.assignment3.models.Character;
import no.accelerate.assignment3.models.Movie;
import no.accelerate.assignment3.services.CrudService;

import java.util.Collection;

/**
 * <h1>CharacterService</h1>
 * An interface which holds the generic operations provided by both the 'Character'-repository and 'CharacterServiceImpl'.
 *
 * @author  Adam Krason
 * @author  Khoi Le
 * @version 1.0
 * @since   2023-02-01
 */
public interface CharacterService extends CrudService<Character, Integer> {
    Character findById(Integer id);
    Collection<Character> findAll();

    /**
     * Executes a query to get all movies a character has starred in.
     *
     * @param characterId The id of the character-entity
     * @return Collection<Movie> A collection of movies the character has starred in
     */
    Collection<Movie> getMovies(Integer characterId);

}
