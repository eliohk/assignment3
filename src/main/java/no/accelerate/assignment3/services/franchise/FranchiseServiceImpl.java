package no.accelerate.assignment3.services.franchise;

import no.accelerate.assignment3.models.Franchise;
import no.accelerate.assignment3.models.Movie;
import no.accelerate.assignment3.models.exceptions.FranchiseNotFoundException;
import no.accelerate.assignment3.repositories.FranchiseRepository;
import no.accelerate.assignment3.repositories.MovieRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * <h1>FranchiseServiceImpl</h1>
 * A class which contain the implementation of operations on the 'franchise'-table and its business logic
 *
 * @author  Adam Krason
 * @author  Khoi Le
 * @version 1.0
 * @since   2023-02-01
 */
@Service
public class FranchiseServiceImpl implements FranchiseService{
    private final FranchiseRepository franchiseRepository;
    private final MovieRepository movieRepository;

    /**
     * An implementation of the FranchiseService needs to have two repositories injected
     * in order to provide the desired functionality and business logic
     *
     * @param franchiseRepository The franchise-repository containing generic operations on a franchise-table
     * @param movieRepository The movie-repository containing generic operations on a movie-table
     */
    public FranchiseServiceImpl(FranchiseRepository franchiseRepository, MovieRepository movieRepository) {
        this.franchiseRepository = franchiseRepository;
        this.movieRepository = movieRepository;
    }

    @Override
    public Franchise findById(Integer id) {
        return franchiseRepository.findById(id).orElseThrow(()
                -> new FranchiseNotFoundException(id));
    }

    @Override
    public Collection<Franchise> findAll() {
        return franchiseRepository.findAll();
    }

    @Override
    public Collection<Movie> getMovies(Integer franchiseId) {
        return franchiseRepository.findById(franchiseId).get().getMovies();
    }

    @Override
    public void updateMovies(int franchiseId, int[] movieIds) {
        Franchise franchise = franchiseRepository.findById(franchiseId).get();
        Set<Movie> movieList = new HashSet<>();

        for (int id: movieIds) {
            movieList.add(movieRepository.findById(id).get());
        }

        movieList.forEach(m -> {
            m.setFranchise(franchise);
            movieRepository.save(m);
        });
    }

    @Override
    public Franchise add(Franchise entity) {
        return franchiseRepository.save(entity);
    }

    @Override
    public Franchise update(Franchise entity) {
        return franchiseRepository.save(entity);
    }

    @Override
    public void deleteById(Integer id) {
        franchiseRepository.deleteById(id);
    }

    @Override
    public boolean exists(Integer id) {
        return franchiseRepository.existsById(id);
    }
}
