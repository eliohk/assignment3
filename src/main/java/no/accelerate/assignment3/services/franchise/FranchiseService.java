package no.accelerate.assignment3.services.franchise;

import no.accelerate.assignment3.models.Franchise;
import no.accelerate.assignment3.models.Movie;
import no.accelerate.assignment3.services.CrudService;

import java.util.Collection;

/**
 * <h1>FranchiseService</h1>
 * An interface which holds the generic operations provided by both the 'Franchise'-repository and 'FranchiseServiceImpl'.
 *
 * @author  Adam Krason
 * @author  Khoi Le
 * @version 1.0
 * @since   2023-02-01
 */
public interface FranchiseService extends CrudService<Franchise, Integer> {
    Franchise findById(Integer id);
    Collection<Franchise> findAll();

    /**
     * Executes a query to get all movies in a franchise.
     *
     * @param franchiseId The id of the franchise
     * @return Collection<Movie> A collection of movies in a franchise
     */
    Collection<Movie> getMovies(Integer franchiseId);

    /**
     * Executes a query to update movies in a franchise.
     *
     * @param franchiseId The id of the franchise-entity to be updated
     * @param movieIds An array containing all the movie id's
     */
    void updateMovies(int franchiseId, int[] movieIds);
}

