package no.accelerate.assignment3.services.movie;

import no.accelerate.assignment3.models.Character;
import no.accelerate.assignment3.models.Franchise;
import no.accelerate.assignment3.models.Movie;
import no.accelerate.assignment3.models.exceptions.MovieNotFoundException;
import no.accelerate.assignment3.repositories.CharacterRepository;
import no.accelerate.assignment3.repositories.MovieRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * <h1>MovieServiceImpl</h1>
 * A class which contain the implementation of operations on the 'movie'-table and its business logic
 *
 * @author  Adam Krason
 * @author  Khoi Le
 * @version 1.0
 * @since   2023-02-01
 */
@Service
public class MovieServiceImpl implements MovieService{
    private final MovieRepository movieRepository;
    private final CharacterRepository characterRepository;

    /**
     * An implementation of the MovieService needs to have two repositories injected
     * in order to provide the desired functionality and business logic
     *
     * @param movieRepository The movie-repository containing generic operations on a movie-table
     * @param characterRepository The character-repository containing generic operations on a character-table
     */
    public MovieServiceImpl(MovieRepository movieRepository, CharacterRepository characterRepository) {
        this.movieRepository = movieRepository;
        this.characterRepository = characterRepository;
    }

    @Override
    public Movie findById(Integer id) {
        return movieRepository.findById(id).orElseThrow(()
                -> new MovieNotFoundException(id));
    }

    @Override
    public Collection<Movie> findAll() {
        return movieRepository.findAll();
    }

    @Override
    public Collection<Character> getCharacters(Integer id) {
        return movieRepository.findById(id).get().getCharacters();
    }

    @Override
    public Movie add(Movie entity) {
        return movieRepository.save(entity);
    }

    @Override
    public Movie update(Movie entity) {
        return movieRepository.save(entity);
    }

    @Override
    public void deleteById(Integer id) {
        movieRepository.deleteById(id);
    }

    @Override
    public boolean exists(Integer id) {
        return movieRepository.existsById(id);
    }

    @Override
    public void updateCharacters(int movieId, int[] characterIds) {
        Movie movie = movieRepository.findById(movieId).get();
        Set<Character> characterList = new HashSet<>();

        for (int id: characterIds) {
            characterList.add(characterRepository.findById(id).get());
        }

        movie.setCharacters(characterList);
        movieRepository.save(movie);
    }
}
