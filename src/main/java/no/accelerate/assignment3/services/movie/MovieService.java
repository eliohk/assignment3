package no.accelerate.assignment3.services.movie;

import no.accelerate.assignment3.models.Character;
import no.accelerate.assignment3.models.Franchise;
import no.accelerate.assignment3.models.Movie;
import no.accelerate.assignment3.services.CrudService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Collection;

/**
 * <h1>MovieService</h1>
 * An interface which holds the generic operations provided by both the 'Movie'-repository and 'MovieServiceImpl'.
 *
 * @author  Adam Krason
 * @author  Khoi Le
 * @version 1.0
 * @since   2023-02-01
 */
public interface MovieService extends CrudService<Movie, Integer> {
    Movie findById(Integer id);
    Collection<Movie> findAll();

    /**
     * Executes a query to get all characters in a movie.
     *
     * @param id The id of the movie
     * @return Collection<Character> A collection of characters in a movie
     */
    Collection<Character> getCharacters(@PathVariable Integer id);

    /**
     * Executes a query to update characters in a movie.
     *
     * @param movieId The id of the movie-entity to be updated
     * @param characterIds An array containing all the character id's
     */
    void updateCharacters(@PathVariable int movieId, @RequestBody int[] characterIds);
}
