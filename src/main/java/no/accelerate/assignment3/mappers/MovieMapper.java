package no.accelerate.assignment3.mappers;

import no.accelerate.assignment3.models.Character;
import no.accelerate.assignment3.models.Franchise;
import no.accelerate.assignment3.models.Movie;
import no.accelerate.assignment3.models.dto.franchises.FranchisePutDTO;
import no.accelerate.assignment3.models.dto.movies.MovieDTO;
import no.accelerate.assignment3.models.dto.movies.MoviePostDTO;
import no.accelerate.assignment3.models.dto.movies.MoviePutDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <h1>MovieMapper</h1>
 * A mapper-class which acts as an assembler, containing logic for creating a 'movie'-DTO
 *
 * @author  Adam Krason
 * @author  Khoi Le
 * @version 1.0
 * @since   2023-02-01
 */
@Mapper(componentModel = "spring")
public interface MovieMapper {
    @Mapping(target = "characters", source = "characters", qualifiedByName = "charactersToCharactersId")
    @Mapping(target = "franchise", source = "franchise", qualifiedByName = "franchiseToFranchiseId")
    MovieDTO movieToMovieDTO(Movie movie);

    Collection<MovieDTO> movieToMovieDTO(Collection<Movie> movie);

    Movie moviePostDTOToMovie(MoviePostDTO moviePostDTO);

    Movie moviePutDTOToMovie(MoviePutDTO moviePutDTO);

    @Named(value = "charactersToCharactersId")
    default Set<Integer> characterMap(Set<Character> value) {
        if (value == null)
            return null;
        return value.stream()
                .map(s -> s.getId())
                .collect(Collectors.toSet());
    }

    @Named(value = "franchiseToFranchiseId")
    default Integer franchiseMap(Franchise value) {
        if (value == null)
            return null;
        return value.getId();
    }
}