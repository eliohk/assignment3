package no.accelerate.assignment3.mappers;
import no.accelerate.assignment3.models.Movie;
import no.accelerate.assignment3.models.dto.characters.CharacterDTO;
import no.accelerate.assignment3.models.dto.characters.CharacterPostDTO;
import no.accelerate.assignment3.models.dto.characters.CharacterPutDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import no.accelerate.assignment3.models.Character;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <h1>CharacterMapper</h1>
 * A mapper-class which acts as an assembler, containing logic for creating a 'character'-DTO
 *
 * @author  Adam Krason
 * @author  Khoi Le
 * @version 1.0
 * @since   2023-02-01
 */
@Mapper(componentModel = "spring")
public interface CharacterMapper {
    @Mapping(target = "movies", source = "movies", qualifiedByName = "moviesToMoviesId")
    CharacterDTO characterToCharacterDTO(Character character);
    Character characterPutDTOToCharacter(CharacterPutDTO characterPutDTO);
    Character characterPostDTOToCharacter(CharacterPostDTO characterPostDTO);
    Collection<CharacterDTO> characterToCharacterDTO(Collection<Character> character);

    @Named(value = "moviesToMoviesId")
    default Set<Integer> map(Set<Movie> value) {
        if (value == null)
            return null;
        return value.stream()
                .map(s -> s.getId())
                .collect(Collectors.toSet());
    }

}
