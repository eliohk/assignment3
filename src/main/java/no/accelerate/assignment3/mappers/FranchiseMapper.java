package no.accelerate.assignment3.mappers;

import no.accelerate.assignment3.models.Franchise;
import no.accelerate.assignment3.models.Movie;
import no.accelerate.assignment3.models.dto.franchises.FranchiseDTO;
import no.accelerate.assignment3.models.dto.franchises.FranchisePostDTO;
import no.accelerate.assignment3.models.dto.franchises.FranchisePutDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <h1>FranchiseMapper</h1>
 * A mapper-class which acts as an assembler, containing logic for creating a 'franchise'-DTO
 *
 * @author  Adam Krason
 * @author  Khoi Le
 * @version 1.0
 * @since   2023-02-01
 */
@Mapper(componentModel = "spring")
public interface FranchiseMapper {
    @Mapping(target = "movies", source = "movies", qualifiedByName = "moviesToMoviesId")
    FranchiseDTO franchiseToFranchiseDTO(Franchise franchise);
    Collection<FranchiseDTO> franchiseToFranchiseDTO(Collection<Franchise> franchise);
    @Mapping(target = "name", source = "name")
    Franchise franchisePostDTOToFranchise(FranchisePostDTO franchisePostDTO);
    @Mapping(target = "id", source = "id")
    Franchise franchisePutDTOToFranchise(FranchisePutDTO franchisePutDTO);

    @Named(value = "moviesToMoviesId")
    default Set<Integer> map(Set<Movie> value) {
        if (value == null)
            return null;
        return value.stream()
                .map(s -> s.getId())
                .collect(Collectors.toSet());
    }

}
