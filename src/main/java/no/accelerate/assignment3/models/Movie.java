package no.accelerate.assignment3.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import jakarta.validation.constraints.Max;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.internal.xml.mapping.MappingXmlParser;

import java.util.Set;

/**
 * <h1>Movie</h1>
 * An entity-object which represent the database table 'Movie'
 *
 * @author  Adam Krason
 * @author  Khoi Le
 * @version 1.0
 * @since   2023-02-01
 */
@Entity
@Getter
@Setter
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(length = 100, nullable = false)
    private String title;

    @Column(length = 30)
    private String genre;

    @Column(length = 10)
    private int release_year;

    @Column(length = 50)
    private String director;

    private String picture;
    private String trailer;

    @ManyToMany
    @JoinTable(
            name = "character_movies",
            joinColumns = {@JoinColumn(name = "movies_id")},
            inverseJoinColumns = {@JoinColumn(name = "characters_id")}
    )
    private Set<Character> characters;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "franchise_id")
    private Franchise franchise;
}
