package no.accelerate.assignment3.models.dto.characters;

import lombok.Getter;
import lombok.Setter;

/**
 * <h1>CharacterPostDTO</h1>
 * A Data Transfer Object (DTO) for the API-model 'Character' to be used when posting to database
 *
 * @author  Adam Krason
 * @author  Khoi Le
 * @version 1.0
 * @since   2023-02-01
 */
@Getter
@Setter
public class CharacterPostDTO {
    private String name;
}
