package no.accelerate.assignment3.models.dto.characters;

import lombok.Getter;
import lombok.Setter;

import java.util.Set;

/**
 * <h1>CharacterDTO</h1>
 * A Data Transfer Object (DTO) for the API-model 'Character'
 *
 * @author  Adam Krason
 * @author  Khoi Le
 * @version 1.0
 * @since   2023-02-01
 */
@Getter
@Setter
public class CharacterDTO {
    private int id;
    private String name;
    private String alias;
    private String gender;
    private String picture;
    private Set<Integer> movies;
}
