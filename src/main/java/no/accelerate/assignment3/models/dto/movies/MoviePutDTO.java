package no.accelerate.assignment3.models.dto.movies;

import lombok.Getter;
import lombok.Setter;
import no.accelerate.assignment3.models.Franchise;

/**
 * <h1>MoviePutDTO</h1>
 * A Data Transfer Object (DTO) for the API-model 'Movie' to be used when updating (put) entity in database
 *
 * @author  Adam Krason
 * @author  Khoi Le
 * @version 1.0
 * @since   2023-02-01
 */
@Getter
@Setter
public class MoviePutDTO {
    private int id;
    private String title;
    private String genre;
    private int release_year;
    private String director;
    private String picture;
    private String trailer;
}
