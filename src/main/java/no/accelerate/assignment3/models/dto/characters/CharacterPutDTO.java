package no.accelerate.assignment3.models.dto.characters;

import lombok.Getter;
import lombok.Setter;

/**
 * <h1>CharacterPutDTO</h1>
 * A Data Transfer Object (DTO) for the API-model 'Character' to be used when updating (put) entity in database
 *
 * @author  Adam Krason
 * @author  Khoi Le
 * @version 1.0
 * @since   2023-02-01
 */
@Getter
@Setter
public class CharacterPutDTO {
    private int id;
    private String name;
    private String alias;
    private String gender;
    private String picture;
}
