package no.accelerate.assignment3.models.dto.franchises;

import lombok.Getter;
import lombok.Setter;

/**
 * <h1>FranchisePutDTO</h1>
 * A Data Transfer Object (DTO) for the API-model 'Franchise' to be used when updating (put) entity in database
 *
 * @author  Adam Krason
 * @author  Khoi Le
 * @version 1.0
 * @since   2023-02-01
 */
@Getter
@Setter
public class FranchisePutDTO {
    private int id;
    private String name;
    private String description;
}
