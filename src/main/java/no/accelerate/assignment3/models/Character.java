package no.accelerate.assignment3.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

/**
 * <h1>Character</h1>
 * An entity-object which represent the database table 'Character'
 *
 * @author  Adam Krason
 * @author  Khoi Le
 * @version 1.0
 * @since   2023-02-01
 */
@Entity
@Getter
@Setter
public class Character {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(length = 50)
    private String name;

    @Column(length = 50)
    private String alias;

    @Column(length = 10)
    private String gender;

    private String picture;

    @JsonIgnore
    @ManyToMany(mappedBy = "characters")
    private Set<Movie> movies;
}

