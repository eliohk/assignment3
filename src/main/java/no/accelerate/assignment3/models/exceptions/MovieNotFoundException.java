package no.accelerate.assignment3.models.exceptions;

/**
 * <h1>MovieNotFoundException</h1>
 * A custom-exception to be thrown when a movie-entity is not found in a database table.
 *
 * @author  Adam Krason
 * @author  Khoi Le
 * @version 1.0
 * @since   2023-02-01
 */
public class MovieNotFoundException extends EntityNotFoundException{
    public MovieNotFoundException(int id) {
        super("Movie with this id does not exist. ID: \" + id");
    }
}
