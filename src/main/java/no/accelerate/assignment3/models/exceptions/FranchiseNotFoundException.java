package no.accelerate.assignment3.models.exceptions;

/**
 * <h1>FranchiseNotFoundException</h1>
 * A custom-exception to be thrown when a franchise-entity is not found in a database table.
 *
 * @author  Adam Krason
 * @author  Khoi Le
 * @version 1.0
 * @since   2023-02-01
 */
public class FranchiseNotFoundException extends EntityNotFoundException{
    public FranchiseNotFoundException(int id) {
        super("Franchise with this id does not exist. ID: \" + id");
    }
}
