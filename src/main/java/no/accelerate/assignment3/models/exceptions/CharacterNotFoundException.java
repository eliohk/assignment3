package no.accelerate.assignment3.models.exceptions;

/**
 * <h1>CharacterNotFoundException</h1>
 * A custom-exception to be thrown when a character-entity is not found in a database table.
 *
 * @author  Adam Krason
 * @author  Khoi Le
 * @version 1.0
 * @since   2023-02-01
 */
public class CharacterNotFoundException extends EntityNotFoundException{
    public CharacterNotFoundException(int id) {
        super("Character with this id does not exist. ID: " + id);
    }
}
