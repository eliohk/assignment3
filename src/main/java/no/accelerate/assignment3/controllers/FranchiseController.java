package no.accelerate.assignment3.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import no.accelerate.assignment3.mappers.CharacterMapper;
import no.accelerate.assignment3.mappers.FranchiseMapper;
import no.accelerate.assignment3.mappers.MovieMapper;
import no.accelerate.assignment3.models.Character;
import no.accelerate.assignment3.models.Franchise;
import no.accelerate.assignment3.models.Movie;
import no.accelerate.assignment3.models.dto.characters.CharacterDTO;
import no.accelerate.assignment3.models.dto.franchises.FranchiseDTO;
import no.accelerate.assignment3.models.dto.franchises.FranchisePostDTO;
import no.accelerate.assignment3.models.dto.franchises.FranchisePutDTO;
import no.accelerate.assignment3.models.dto.movies.MovieDTO;
import no.accelerate.assignment3.services.franchise.FranchiseService;
import org.springframework.http.ProblemDetail;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashSet;
import java.util.Set;

/**
 * <h1>FranchiseController</h1>
 * A controller-class which acts as an extra layer of abstraction handling user interaction with the 'franchise'-table
 *
 * @author  Adam Krason
 * @author  Khoi Le
 * @version 1.0
 * @since   2023-02-01
 */
@RestController
@RequestMapping(path = "api/v1/franchises")
public class FranchiseController {
    private final FranchiseService franchiseService;
    private final FranchiseMapper franchiseMapper;
    private final MovieMapper movieMapper;
    private final CharacterMapper characterMapper;

    /**
     * An implementation of the FranchiseController needs to have services and mappers injected
     * to be able to handle user interaction.
     *
     * @param franchiseService The franchise service which holds business logic and manipulates database data
     * @param franchiseMapper A mapper which contain logic for constructing DTOs for franchises
     * @param movieMapper A mapper which contain logic for constructing DTOs for movies
     * @param characterMapper A mapper which contain logic for constructing DTOs for characters
     */
    public FranchiseController(FranchiseService franchiseService, FranchiseMapper franchiseMapper, MovieMapper movieMapper, CharacterMapper characterMapper) {
        this.franchiseService = franchiseService;
        this.franchiseMapper = franchiseMapper;
        this.movieMapper = movieMapper;
        this.characterMapper = characterMapper;
    }

    @Operation(summary = "Gets franchise by ID")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Success",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = FranchiseDTO.class))
                    }
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProblemDetail.class))
            )
    })
    @GetMapping("{id}")
    public ResponseEntity findById(@PathVariable Integer id) {
        return ResponseEntity.ok(
                franchiseMapper.franchiseToFranchiseDTO(franchiseService.findById(id))
        );
    }

    @Operation(summary = "Gets all the franchises")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Success",
                    content = {
                            @Content(mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = FranchiseDTO.class)))
                    }
            )
    })
    @GetMapping
    public ResponseEntity findAll() {
        return ResponseEntity.ok(
                franchiseMapper.franchiseToFranchiseDTO(franchiseService.findAll())
        );
    }

    @Operation(summary = "Adds a new franchise")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "201",
                    description = "Created",
                    content = @Content
            )
    })
    @PostMapping
    public ResponseEntity add(@RequestBody FranchisePostDTO entity) throws URISyntaxException {
        Franchise franchise = franchiseMapper.franchisePostDTOToFranchise(entity);
        franchiseService.add(
                franchise
        );
        URI uri =  new URI("api/v1/characters/" + franchise.getId());
        return ResponseEntity.created(uri).build();
    }

    @Operation(summary = "Updates a franchise")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "204",
                    description = "Success",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad Request",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found",
                    content = @Content
            )
    })
    @PutMapping("{id}")
    public ResponseEntity update(@RequestBody FranchisePutDTO entity, @PathVariable Integer id) {
        if(id != entity.getId())
            return ResponseEntity.badRequest().build();
        franchiseService.update(
                franchiseMapper.franchisePutDTOToFranchise(entity)
        );
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Gets all the movies in the franchise")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Success",
                    content = {
                            @Content(mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = MovieDTO.class)))
                    }
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProblemDetail.class))
            )
    })
    @GetMapping("{id}/movies")
    public ResponseEntity getMovies(@PathVariable Integer id) {
        return ResponseEntity.ok(
                movieMapper.movieToMovieDTO(franchiseService.getMovies(id))
        );
    }

    @Operation(summary = "Gets all the characters that star in a movie " +
            "belonging to the given franchise")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Success",
                    content = {
                            @Content(mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = CharacterDTO.class)))
                    }
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProblemDetail.class))
            )
    })
    @GetMapping("{id}/characters")
    public ResponseEntity getCharactersInFranchise(@PathVariable Integer id) {
        Set<Movie> movies = franchiseService.findById(id).getMovies();
        Set<Character> characters = new HashSet<>();
        for (Movie movie: movies){
            characters.addAll(movie.getCharacters());
        }
        return ResponseEntity.ok(
                characterMapper.characterToCharacterDTO(characters)
        );
    }

    @Operation(summary = "Updates the movies in the franchise")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "204",
                    description = "Success",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad Request",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found",
                    content = @Content
            )
    })
    @PutMapping("{id}/movies")
    public ResponseEntity updateMovies(@PathVariable int id, @RequestBody int[] movieIds) {
        franchiseService.updateMovies(id, movieIds);
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Deletes the franchise with the given ID")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "204",
                    description = "No content",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found",
                    content = @Content
            )
    })
    @DeleteMapping("{id}")
    public ResponseEntity delete(@PathVariable int id) {
        Franchise franchise = franchiseService.findById(id);
        for (Movie movie : franchise.getMovies()) {
            movie.setFranchise(null);
        }
        franchise.setMovies(null);
        franchiseService.deleteById(id);
        return ResponseEntity.noContent().build();
    }
}
