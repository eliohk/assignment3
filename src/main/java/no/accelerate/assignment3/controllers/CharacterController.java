package no.accelerate.assignment3.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import no.accelerate.assignment3.mappers.CharacterMapper;
import no.accelerate.assignment3.mappers.MovieMapper;
import no.accelerate.assignment3.models.Character;
import no.accelerate.assignment3.models.Movie;
import no.accelerate.assignment3.models.dto.characters.CharacterDTO;
import no.accelerate.assignment3.models.dto.characters.CharacterPostDTO;
import no.accelerate.assignment3.models.dto.characters.CharacterPutDTO;
import no.accelerate.assignment3.services.character.CharacterService;
import org.springframework.http.ProblemDetail;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Set;

/**
 * <h1>CharacterController</h1>
 * A controller-class which acts as an extra layer of abstraction handling user interaction with the 'character'-table
 *
 * @author  Adam Krason
 * @author  Khoi Le
 * @version 1.0
 * @since   2023-02-01
 */
@RestController
@RequestMapping(path = "api/v1/characters")
public class CharacterController {
    private final CharacterService characterService;
    private final CharacterMapper characterMapper;
    private final MovieMapper movieMapper;

    /**
     * An implementation of the CharacterController needs to have services and mappers injected
     * to be able to handle user interaction.
     *
     * @param characterService The character service which holds business logic and manipulates database data
     * @param characterMapper A mapper which contain logic for constructing DTOs for characters
     * @param movieMapper A mapper which contain logic for constructing DTOs for movies
     */
    public CharacterController(CharacterService characterService, CharacterMapper characterMapper, MovieMapper movieMapper) {
        this.characterService = characterService;
        this.characterMapper = characterMapper;
        this.movieMapper = movieMapper;
    }

    @Operation(summary = "Gets character by ID")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Success",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = CharacterDTO.class))
                    }
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProblemDetail.class))
            )
    })
    @GetMapping("{id}")
    public ResponseEntity findById(@PathVariable Integer id) {
        return ResponseEntity.ok(
                characterMapper.characterToCharacterDTO(characterService.findById(id))
        );
    }

    @Operation(summary = "Gets all the characters")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Success",
                    content = {
                            @Content(mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = CharacterDTO.class)))
                    }
            )
    })
    @GetMapping
    public ResponseEntity findAll() {
        return ResponseEntity.ok(
                characterMapper.characterToCharacterDTO(characterService.findAll()));
    }

    @Operation(summary = "Adds a new character")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "201",
                    description = "Created",
                    content = @Content
            )
    })
    @PostMapping
    public ResponseEntity add(@RequestBody CharacterPostDTO entity) throws URISyntaxException {
        Character character = characterMapper.characterPostDTOToCharacter(entity);
        characterService.add(
                character
        );
        URI uri =  new URI("api/v1/characters/" + character.getId());
        return ResponseEntity.created(uri).build();
    }

    @PutMapping("{id}")
    @Operation(summary = "Updates a character")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "204",
                    description = "Success",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad Request",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found",
                    content = @Content
            )
    })
    public ResponseEntity update(@RequestBody CharacterPutDTO entity, @PathVariable Integer id) {
        if(id != entity.getId())
            return ResponseEntity.badRequest().build();
        characterService.update(
                characterMapper.characterPutDTOToCharacter(entity)
        );
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Gets all the movies the character has a role in")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Success",
                    content = {
                            @Content(mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = CharacterDTO.class)))
                    }
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProblemDetail.class))
            )
    })
    @GetMapping("{id}/movies")
    public ResponseEntity getMovies(@PathVariable Integer id) {
        return ResponseEntity.ok(
                movieMapper.movieToMovieDTO(characterService.getMovies(id))
        );
    }

    @Operation(summary = "Deletes the character with the given ID")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "204",
                    description = "No content",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found",
                    content = @Content
            )
    })
    @DeleteMapping("{id}")
    public ResponseEntity delete(@PathVariable int id){
        Character character = characterService.findById(id);
        for (Movie movie: character.getMovies()) {
            Set<Character> characterSet = movie.getCharacters();
            characterSet.remove(character);
        }
        character.setMovies(null);
        characterService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

}