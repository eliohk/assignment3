package no.accelerate.assignment3.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import no.accelerate.assignment3.mappers.CharacterMapper;
import no.accelerate.assignment3.mappers.MovieMapper;
import no.accelerate.assignment3.models.Character;
import no.accelerate.assignment3.models.Franchise;
import no.accelerate.assignment3.models.Movie;
import no.accelerate.assignment3.models.dto.characters.CharacterDTO;
import no.accelerate.assignment3.models.dto.movies.MovieDTO;
import no.accelerate.assignment3.models.dto.movies.MoviePostDTO;
import no.accelerate.assignment3.models.dto.movies.MoviePutDTO;
import no.accelerate.assignment3.services.movie.MovieService;
import org.springframework.http.ProblemDetail;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Set;

/**
 * <h1>MovieController</h1>
 * A controller-class which acts as an extra layer of abstraction handling user interaction with the 'movie'-table
 *
 * @author  Adam Krason
 * @author  Khoi Le
 * @version 1.0
 * @since   2023-02-01
 */
@RestController
@RequestMapping(path = "api/v1/movies")
public class MovieController {
    private final MovieService movieService;
    private final MovieMapper movieMapper;
    private final CharacterMapper characterMapper;

    /**
     * An implementation of the MovieController needs to have services and mappers injected
     * to be able to handle user interaction.
     *
     * @param movieService The movie service which holds business logic and manipulates database data
     * @param movieMapper A mapper which contain logic for constructing DTOs for movies
     * @param characterMapper A mapper which contain logic for constructing DTOs for characters
     */
    public MovieController(MovieService movieService, MovieMapper movieMapper, CharacterMapper characterMapper) {
        this.movieService = movieService;
        this.movieMapper = movieMapper;
        this.characterMapper = characterMapper;
    }

    @Operation(summary = "Gets movie by ID")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Success",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = MovieDTO.class))
                    }
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProblemDetail.class))
            )
    })
    @GetMapping("{id}")
    public ResponseEntity findById(@PathVariable Integer id) {
        return ResponseEntity.ok(
                movieMapper.movieToMovieDTO(movieService.findById(id))
        );
    }

    @Operation(summary = "Gets all the movies")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Success",
                    content = {
                            @Content(mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = CharacterDTO.class)))
                    }
            )
    })
    @GetMapping
    public ResponseEntity findAll() {
        return ResponseEntity.ok(
                movieMapper.movieToMovieDTO(movieService.findAll())
        );
    }

    @Operation(summary = "Adds a new movie")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "201",
                    description = "Created",
                    content = @Content
            )
    })
    @PostMapping
    public ResponseEntity add(@RequestBody MoviePostDTO entity) throws URISyntaxException {
        Movie movie = movieMapper.moviePostDTOToMovie(entity);
        movieService.add(movie);
        URI uri =  new URI("api/v1/movies/" + movie.getId());
        return ResponseEntity.created(uri).build();
    }

    @Operation(summary = "Updates a character")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "204",
                    description = "Success",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad Request",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found",
                    content = @Content
            )
    })
    @PutMapping("{id}")
    public ResponseEntity update(@RequestBody MoviePutDTO entity, @PathVariable Integer id) {
        if(id != entity.getId()) {
            return ResponseEntity.badRequest().build();
        }

        movieService.update(
                movieMapper.moviePutDTOToMovie(entity)
        );
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Gets all the movies the character has a role in")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Success",
                    content = {
                            @Content(mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = CharacterDTO.class)))
                    }
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProblemDetail.class))
            )
    })
    @GetMapping("{id}/characters")
    public ResponseEntity getCharacters(@PathVariable Integer id) {
        return ResponseEntity.ok(
                characterMapper.characterToCharacterDTO(movieService.getCharacters(id))
        );
    }

    @Operation(summary = "Updates the characters that star in the movie")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "204",
                    description = "Success",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad Request",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found",
                    content = @Content
            )
    })
    @PutMapping("{id}/characters")
    public ResponseEntity updateCharacters(@PathVariable int id, @RequestBody int[] characterIds) {
        movieService.updateCharacters(id, characterIds);
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Deletes the movie with the given ID")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "204",
                    description = "No content",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found",
                    content = @Content
            )
    })
    @DeleteMapping("{id}")
    public ResponseEntity delete(@PathVariable int id){
        Movie movie = movieService.findById(id);
        for (Character character : movie.getCharacters()){
            Set<Movie> movies = character.getMovies();
            movies.remove(movie);
        }
        movie.setCharacters(null);
        Franchise franchise = movie.getFranchise();
        franchise.getMovies().remove(movie);
        movie.setFranchise(null);
        movieService.deleteById(id);
        return ResponseEntity.noContent().build();
    }
}
