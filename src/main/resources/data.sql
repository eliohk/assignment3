-- Franchises
INSERT INTO franchise (name, description) VALUES ('Star Wars', 'Film series with lightsabers and cool spaceships.');
INSERT INTO franchise (name, description) VALUES ('Spider-Man', 'Guy climbing walls');

-- Characters
INSERT INTO character (name, alias, gender, picture)
VALUES ('Anakin Skywalker', 'Darth Vader', 'male',
        'https://static.wikia.nocookie.net/starwars/images/5/59/Anakin_skywalker.jpg/revision/latest/scale-to-width-down/275?cb=20080428120001&path-prefix=no');
INSERT INTO character (name, alias, gender, picture)
VALUES ('Sheev Palpatine', 'The Emperor', 'male',
        'https://static.wikia.nocookie.net/starwars/images/d/d8/Emperor_Sidious.png/revision/latest?cb=20130620100935');
INSERT INTO character (name, alias, gender, picture)
VALUES ('Padme Amidala', 'Padme', 'female',
        'https://static.wikia.nocookie.net/starwars/images/b/b2/Padmegreenscrshot.jpg/revision/latest?cb=20100423143631');
INSERT INTO character (name, alias, gender, picture)
VALUES ('Peter Parker', 'Spider-Man', 'male',
        'https://substackcdn.com/image/fetch/w_1456,c_limit,f_webp,q_auto:good,fl_progressive:steep/https%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2F7db254f7-79aa-4a17-aad4-ca6515c7cbc8_1200x675.jpeg');
INSERT INTO character (name, alias, gender, picture)
VALUES ('Norman Osborn', 'Green Goblin', 'male',
        'https://www.writeups.org/wp-content/uploads/Green-Goblin-Spider-Man-movie-Osborn-Willem-Dafoe-l.jpg');
INSERT INTO character (name, alias, gender, picture)
VALUES ('Otto Otavius', 'Doctor Octopus', 'male',
        'https://static.wikia.nocookie.net/spiderman-films/images/3/37/Doctor_Octopus.jpg/revision/latest/scale-to-width-down/1200?cb=20130120100202');

-- Movies
INSERT INTO movie (title, genre, release_year, director, picture, trailer, franchise_id)
VALUES ('Episode 1: The Phantom Menace', 'Sci-Fi', 1999, 'George Lucas',
        'https://m.media-amazon.com/images/M/MV5BYTRhNjcwNWQtMGJmMi00NmQyLWE2YzItODVmMTdjNWI0ZDA2XkEyXkFqcGdeQXVyNTAyODkwOQ@@._V1_.jpg',
        'https://youtu.be/bD7bpG-zDJQ',
        2);
INSERT INTO movie (title, genre, release_year, director, picture, trailer, franchise_id)
VALUES ('Episode 2: Attack of the Clones', 'Sci-Fi', 2002, 'George Lucas',
        'https://upload.wikimedia.org/wikipedia/en/3/32/Star_Wars_-_Episode_II_Attack_of_the_Clones_%28movie_poster%29.jpg',
        'https://www.youtube.com/watch?v=gYbW1F_c9eM',
        2);
INSERT INTO movie (title, genre, release_year, director, picture, trailer, franchise_id)
VALUES ('Episode 3: Revenge of the Sith', 'Sci-Fi', 2005, 'George Lucas',
        'https://m.media-amazon.com/images/M/MV5BNTc4MTc3NTQ5OF5BMl5BanBnXkFtZTcwOTg0NjI4NA@@._V1_.jpg',
            'https://www.youtube.com/watch?v=5UnjrG_N8hU',
        2);
INSERT INTO movie (title, genre, release_year, director, picture, trailer, franchise_id)
VALUES ('Spider-Man', 'Sci-Fi', 2002, 'Sam Raimi',
        'https://upload.wikimedia.org/wikipedia/en/f/f3/Spider-Man2002Poster.jpg',
        'https://www.youtube.com/watch?v=t06RUxPbp_c',
        1);
INSERT INTO movie (title, genre, release_year, director, picture, trailer, franchise_id)
VALUES ('Spider-Man 2', 'Sci-Fi', 2004, 'Sam Raimi',
        'https://upload.wikimedia.org/wikipedia/en/0/02/Spider-Man_2_Poster.jpg',
        'https://www.youtube.com/watch?v=1s9Yln0YwCw',
        1);
INSERT INTO movie (title, genre, release_year, director, picture, trailer, franchise_id)
VALUES ('Spider-Man 2', 'Sci-Fi', 2004, 'Sam Raimi',
        'https://upload.wikimedia.org/wikipedia/en/0/02/Spider-Man_2_Poster.jpg',
        'https://www.youtube.com/watch?v=1s9Yln0YwCw',
        1);
INSERT INTO movie (title, genre, release_year, director, picture, trailer, franchise_id)
VALUES ('Spider-Man 3', 'Sci-Fi', 2007, 'Sam Raimi',
        'https://upload.wikimedia.org/wikipedia/en/7/7a/Spider-Man_3%2C_International_Poster.jpg',
        'https://www.youtube.com/watch?v=e5wUilOeOmg',
        1);

-- Characters in movies
INSERT INTO character_movies (characters_id, movies_id) VALUES (1, 1);
INSERT INTO character_movies (characters_id, movies_id) VALUES (2, 1);
INSERT INTO character_movies (characters_id, movies_id) VALUES (1, 2);
INSERT INTO character_movies (characters_id, movies_id) VALUES (2, 2);
INSERT INTO character_movies (characters_id, movies_id) VALUES (3, 2);
INSERT INTO character_movies (characters_id, movies_id) VALUES (5, 3);
INSERT INTO character_movies (characters_id, movies_id) VALUES (6, 3);
INSERT INTO character_movies (characters_id, movies_id) VALUES (5, 4);
INSERT INTO character_movies (characters_id, movies_id) VALUES (6, 4);

